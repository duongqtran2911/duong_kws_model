import torch.nn as nn
import torchaudio
import torch
import librosa

class CustomModel(nn.Module):
    def __init__(self):
        super().__init__()
        # input -> mel -> CNN -> RNN -> Linear -> softmax -> output
        self.mel = torchaudio.transforms.MelSpectrogram(n_fft=1024, win_length=512, n_mels=50)
        # [1, 50, 32] -> B = 1, F = 50, T = 32
        # B(batch_size -> N) x F(n_mels -> H_in) x T(time -> L)  
        self.cnn1 = nn.Conv2d(1, 8, kernel_size=3, stride=2)
        self.cnn2 = nn.Conv2d(8, 4, kernel_size=3, stride=1)
        
        # T x B x F when batch_first=False and B x T x F when batch_first=True
        self.rnn = nn.LSTM(88, 64, batch_first=True)
        self.linear = nn.Linear(64, 16)
        num_classes = 30
        self.classifier = nn.Linear(29, num_classes)

    def forward(self, x):
        x = self.mel(x)
        # print(x.shape)
        x = x.unsqueeze(1)
        x = self.cnn1(x)
        x = self.cnn2(x)
        # T x B x F when batch_first=False and B x T x F when batch_first=True
        x = x.view(x.shape[0], -1, x.shape[-1])
        x = x.transpose(1, 2)
        x = self.rnn(x)[0]
        x = self.linear(x)
        x = x.sum(dim=-1)
        x = self.classifier(x) 
        x = x.softmax(-1)
        x = x.argmax(-1)
        return x

if __name__ == '__main__':
    model = CustomModel()
    print(model)

    x = torch.randn(3, 20, 16000)
    x = x.unsqueeze(1) # in_channels=1
    y = model(x)
    print(y.shape)
    
    # wf, sr = librosa.load('GSCdata\bed\0a7c2a8d_nohash_0.wav', sr=None)
    # mel = torchaudio.transforms.MelSpectrogram(sr)(wf)
    # print(mel.shape)
