import torch.optim as optim
import torchaudio
import torchaudio.functional as F
import torchaudio.transforms as T

from model import CustomModel
import util
from dataloader import DataLoader, GSCDataset


def main():
    dataset = GSCDataset()
    dataloader = DataLoader(dataset=dataset, 
                                batch_size=1, 
                                shuffle=True,
                                drop_last=True)
    model = CustomModel()
    model.train()
    # print(len(dataset.paths))
    # print(len(dataset.labels))
    
    for batch in dataloader:
        inputs, labels = batch
        print(inputs, labels)
        preds = model(inputs)
        print(preds)
        exit()
        optimizer = optim.Adam(model.parameters(), lr=0.01, weight_decay=0.0001)
        # loss = criterion(labels, preds)
        loss = F.nll_loss(preds, labels)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()


if __name__ == '__main__':
    main()



