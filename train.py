# from math import tan
import torch
import torchaudio
from torchaudio.datasets.speechcommands import SPEECHCOMMANDS
import torchaudio.functional as F
import torchaudio.transforms as T
import torch.nn as nn
import torch.optim as optim
import random
# from torch.utils.data import TensorDataset
# from pathlib import Path

from model import CustomModel
from dataloader import GSCDataset, DataLoader
import util

def train():
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    dataset = GSCDataset()
    # print(dataset.labels)
    # exit()
    # a = TensorDataset(dataset)
    # a = random.sample(a)
    dataloader = DataLoader(dataset=dataset, batch_size=3, shuffle=True, drop_last=True)
    model = CustomModel()
    optimizer = optim.Adam(model.parameters(), lr=0.01, weight_decay=0.0001)
    model.train()
    # print(len(dataset.paths))
    # print(len(dataset.labels))
    
    for batch in dataloader:
        print('batch', batch, 'batch type', type(batch))
        print('batch length\n', len(batch))
        inputs, labels = batch
        # inputfile = torchaudio.load(inputs)
        # inputfile = inputfile.to(device)
        inputs = inputs.to(device)
        labels = labels.to(device)
        labels = labels.float()
        print('inputs\n', inputs)
        print('labels\n', labels)
        preds = model(inputs)
        preds = preds.float()
        print('preds\n', preds)
        # loss = criterion(labels, preds)
        # exit()
        loss = nn.MSELoss(preds, labels)
        # output = loss(preds, labels)
        optimizer.zero_grad()
        # output.backward()
        loss.backward()
        optimizer.step()
        
if __name__ == '__main__':
    train()