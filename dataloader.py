from torch.utils.data import DataLoader, Dataset
import torchaudio
from torchaudio.datasets import SPEECHCOMMANDS
from tqdm import tqdm
import numpy as np
import torch
import os
import random

from util import recursive_walk

class GSCDataset(Dataset):
    def __init__(self):
        self.paths = [f for f in recursive_walk('GSCdata') if f.endswith('.wav') 
                                                        and '_background_noise_' not in f]
        self.labels = sorted(list( ['five',
                                    'marvin',
                                    'right',
                                    'tree',
                                    'bed',
                                    'four',
                                    'nine',
                                    'seven',
                                    'two',
                                    'bird',
                                    'go',
                                    'no',
                                    'sheila',
                                    'up',
                                    'cat',
                                    'happy',
                                    'off',
                                    'six',
                                    'dog',
                                    'house',
                                    'on',
                                    'stop',
                                    'wow',
                                    'down',
                                    'left',
                                    'one',
                                    'yes',
                                    'eight',
                                    'three',
                                    'zero',] ))

    def __len__(self):
        return len(self.paths)

    def __getitem__(self, idx):
        label = self.paths[idx].split('\\')[-2]
        data, fs = torchaudio.load(self.paths[idx], normalize=True)
        # print(data.shape())
        print('old data:', data, 'data len:', len(data[0]))
        if len(data[0]) > 16000: 
            data[0] = data[0][:16000]
        elif len(data[0]) < 16000:
            tmp = torch.zeros([16000 - len(data[0])])
            data[0] = torch.cat([data[0], tmp])
        print('new data:', data, 'data len:', len(data[0]))
        return data[0], self.labels.index(label)


# if __name__ == "__main__":
#     dataset = GSCDataset()
#     # random.seed(a=60000)
#     print(dataset.__getitem__(random.randint(0,60000)))

# if data.shape[0] > 16000: 
#     data = data[:16000]
# elif data.shape[0] < 16000:
#     tmp = torch.zeros([16000 - data.shape[0],])
#     data = torch.cat([data, tmp])
    